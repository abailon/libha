/*
 * Copyright (C) 2022 Alexandre Bailon <abailon@baylibre.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <confuse.h>
#include <json-c/json.h>

#include <ha.h>

#include "munit/munit.h"

#define FNAME_MAX_SIZE  (256)

struct ha_config {
	char *url;
	int port;
	char *token;
};

static const char *test_json = "{\"entity_id\":\"binary_sensor.always_on_sensor\",\"state\":\"on\",\"attributes\":{\"friendly_name\":\"A sensor always on\"},\"last_changed\":\"2022-09-20T09:39:53.767372+00:00\",\"last_updated\":\"2022-09-20T09:39:53.767372+00:00\",\"context\":{\"id\":\"01GDD49B97Q843X8CZA8Y6S7WE\",\"parent_id\":null,\"user_id\":null}}";
static const char *invalid_json = "{\"message\":\"Entity not found.\"}";
static const char *climate_json = "{\"entity_id\":\"climate.test_climate\",\"state\":\"off\",\"attributes\":{\"hvac_modes\":[\"heat\",\"off\"],\"min_temp\":7,\"max_temp\":35,\"target_temp_step\":0.1,\"current_temperature\":25.0,\"temperature\":7.0,\"hvac_action\":\"off\",\"friendly_name\":\"Test climate\",\"supported_features\":1},\"last_changed\":\"2022-09-21T07:36:59.143952+00:00\",\"last_updated\":\"2022-09-21T07:37:00.410629+00:00\",\"context\":{\"id\":\"01GDFFN1QT4SGCH4X4NE26QD3E\",\"parent_id\":null,\"user_id\":null}}";
static struct homeassistant *g_ha;
static struct json_object *test_json_obj;
static struct json_object *invalid_json_obj;
static struct json_object *climate_json_obj;

static void abortHandler(int signal) {
    ha_stop(g_ha);
}

static int load_config(struct ha_config *config)
{
	struct stat st;
	cfg_t *cfg;

	if (stat("ha_client.conf" ,&st)) {
	    printf("ha_client.conf is missing!\n");
	    return ENOENT;
	}

	cfg_opt_t opts[] = {
		CFG_STR("url", "", CFGF_NONE),
		CFG_INT("port", 443, CFGF_NONE),
		CFG_STR("token", "", CFGF_NONE),
		CFG_END()
	};

	cfg = cfg_init(opts, CFGF_NONE);
	if (cfg_parse(cfg, "ha_client.conf") == CFG_PARSE_ERROR)
		return 1;

	config->url = strdup(cfg_getstr(cfg, "url"));
	config->port = cfg_getint(cfg, "port");
	config->token = strdup(cfg_getstr(cfg, "token"));

	cfg_free(cfg);

	return 0;
}

static void release_config(struct ha_config *config)
{
	free(config->url);
	free(config->token);
}

void expired(union sigval timer_data) {
    struct homeassistant *ha = timer_data.sival_ptr;
	static int i;

	if (i == 0)
		ha_switch_turn_on(g_ha, "switch.test_switch");
	else if (i == 1)
		ha_switch_turn_off(g_ha, "switch.test_switch");
	else
		ha_stop(ha);
	i += 1;
}

int ha_test_timer(struct homeassistant *ha) {
    int res = 0;
    timer_t timerId = 0;

    /*  sigevent specifies behaviour on expiration  */
    struct sigevent sev = { 0 };

    /* specify start delay and interval
     * it_value and it_interval must not be zero */

    struct itimerspec its = {
		.it_value.tv_sec  = 1,
		.it_value.tv_nsec = 0,
		.it_interval.tv_sec  = 1,
		.it_interval.tv_nsec = 0
	};

    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_function = &expired;
    sev.sigev_value.sival_ptr = ha;

    /* create timer */
    res = timer_create(CLOCK_REALTIME, &sev, &timerId);
    if (res != 0){
        fprintf(stderr, "Error timer_create: %s\n", strerror(errno));
        exit(-1);
    }

    /* start timer */
    res = timer_settime(timerId, 0, &its, NULL);
    if (res != 0){
        fprintf(stderr, "Error timer_settime: %s\n", strerror(errno));
        exit(-1);
    }

    return 0;
}

MunitResult ha_init_test(const MunitParameter params[], void* user_data_or_fixture)
{
	struct homeassistant *ha;
	struct ha_config config;
	int ret;

	ret = load_config(&config);
	munit_assert_int(ret, ==, 0);

	ha = ha_init(NULL, 0, NULL);
	munit_assert_null(ha);

	ha = ha_init(config.url, 0, NULL);
	munit_assert_null(ha);

	ha = ha_init(config.url, config.port, NULL);
	munit_assert_null(ha);

	ha = ha_init(config.url, config.port, config.token);
	munit_assert_not_null(ha);

	ha_stop(ha);

	ha = ha_init(config.url, config.port, "bad token");
	munit_assert_null(ha);

	ha = ha_init("http://bad-url.bad", config.port, config.token);
	munit_assert_null(ha);

	ha = ha_init("https://www.google.com", 443, config.token);
	munit_assert_null(ha);

	ha_exit(ha);

	return MUNIT_OK;
}

MunitResult my_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *res;

	res = ha_get_state(NULL, "binary_sensor.always_invalid_sensor");
	munit_assert_null(res);

	res = ha_get_state(g_ha, NULL);
	munit_assert_null(res);

	res = ha_get_state(g_ha, "binary_sensor.always_on_sensor");
	munit_assert_not_null(res);
	munit_assert_string_equal(res, "on");

	res = ha_get_state(g_ha, "binary_sensor.always_off_sensor");
	munit_assert_not_null(res);
	munit_assert_string_equal(res, "off");

	res = ha_get_state(g_ha, "binary_sensor.always_invalid_sensor");
	munit_assert_null(res);

	return MUNIT_OK;
}

MunitResult ha_get_states_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *res;

	res = ha_get_states(NULL);
	munit_assert_null(res);

	res = ha_get_states(g_ha);
	munit_assert_not_null(res);
	munit_assert_not_null(strstr(res, "binary_sensor.always_on_sensor"));

	return MUNIT_OK;
}

MunitResult ha_light_turn_on_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int ret;
	const char *value;

	ret = ha_light_turn_on(NULL, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_light_turn_on(g_ha, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_light_turn_on(g_ha, "light.test_lights");
	munit_assert_int(ret, ==, 0);

	value = ha_get_state(g_ha, "light.test_lights");
	munit_assert_not_null(value);
	munit_assert_string_equal(value, "on");

	ret = ha_light_turn_on(NULL, "light.test_lights");
	munit_assert_int(ret, !=, 0);

	return MUNIT_OK;
}


MunitResult ha_light_turn_off_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int ret;
	const char *value;

	ret = ha_light_turn_off(NULL, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_light_turn_off(g_ha, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_light_turn_off(g_ha, "light.test_lights");
	munit_assert_int(ret, ==, 0);

	value = ha_get_state(g_ha, "light.test_lights");
	munit_assert_not_null(value);
	munit_assert_string_equal(value, "off");

	ret = ha_light_turn_off(NULL, "light.test_lights");
	munit_assert_int(ret, !=, 0);

	return MUNIT_OK;
}

MunitResult ha_switch_turn_on_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int ret;
	const char *value;

	ret = ha_switch_turn_on(NULL, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_switch_turn_on(g_ha, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_switch_turn_on(g_ha, "switch.test_switch");
	munit_assert_int(ret, ==, 0);

	value = ha_get_state(g_ha, "switch.test_switch");
	munit_assert_not_null(value);
	munit_assert_string_equal(value, "on");

	ret = ha_switch_turn_on(NULL, "switch.test_switch");
	munit_assert_int(ret, !=, 0);

	return MUNIT_OK;
}

MunitResult ha_switch_turn_off_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int ret;
	const char *value;

	ret = ha_switch_turn_off(NULL, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_switch_turn_off(g_ha, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_switch_turn_off(g_ha, "switch.test_switch");
	munit_assert_int(ret, ==, 0);

	value = ha_get_state(g_ha, "switch.test_switch");
	munit_assert_not_null(value);
	munit_assert_string_equal(value, "off");

	ret = ha_switch_turn_off(NULL, "switch.test_switch");
	munit_assert_int(ret, !=, 0);

	return MUNIT_OK;
}

MunitResult ha_climate_turn_on_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int ret;

	ret = ha_climate_turn_on(NULL, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_climate_turn_on(g_ha, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_climate_turn_on(g_ha, "climate.test_climate");
	munit_assert_int(ret, ==, 0);

	ret = ha_climate_turn_on(NULL, "climate.test_climate");
	munit_assert_int(ret, !=, 0);

	return MUNIT_OK;
}

MunitResult ha_climate_turn_off_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int ret;

	ret = ha_climate_turn_off(NULL, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_climate_turn_off(g_ha, NULL);
	munit_assert_int(ret, !=, 0);

	ret = ha_climate_turn_off(g_ha, "climate.test_climate");
	munit_assert_int(ret, ==, 0);

	ret = ha_climate_turn_off(NULL, "climate.test_climate");
	munit_assert_int(ret, !=, 0);

	return MUNIT_OK;
}

MunitResult ha_entity_id_to_domain_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *domain;

	domain = ha_entity_id_to_domain(NULL);
	munit_assert_null(domain);

	domain = ha_entity_id_to_domain("invalid.domain.entity_id");
	munit_assert_null(domain);

	domain = ha_entity_id_to_domain("switch.entity_id");
	munit_assert_not_null(domain);
	munit_assert_string_equal(domain, "switch");

	return MUNIT_OK;
}

MunitResult ha_get_friendly_name_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *name;

	name = ha_get_friendly_name(NULL);
	munit_assert_null(name);

	name = ha_get_friendly_name(invalid_json_obj);
	munit_assert_null(name);

	name = ha_get_friendly_name(test_json_obj);
	munit_assert_not_null(name);
	munit_assert_string_equal(name, "A sensor always on");


	return MUNIT_OK;
}

MunitResult ha_get_entity_id_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *name;

	name = ha_get_entity_id(NULL);
	munit_assert_null(name);

	name = ha_get_entity_id(invalid_json_obj);
	munit_assert_null(name);

	name = ha_get_entity_id(test_json_obj);
	munit_assert_not_null(name);
	munit_assert_string_equal(name, "binary_sensor.always_on_sensor");

	return MUNIT_OK;
}

MunitResult ha_get_entity_state_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *state;

	state = ha_get_entity_state(NULL);
	munit_assert_null(state);

	state = ha_get_entity_state(invalid_json_obj);
	munit_assert_null(state);

	state = ha_get_entity_state(test_json_obj);
	munit_assert_not_null(state);
	munit_assert_string_equal(state, "on");

	return MUNIT_OK;
}

MunitResult ha_get_entity_attr_str_test(const MunitParameter params[], void* user_data_or_fixture)
{
	const char *name;

	name = ha_get_entity_attr_str(NULL, NULL);
	munit_assert_null(name);

	name = ha_get_entity_attr_str(invalid_json_obj, NULL);
	munit_assert_null(name);


	name = ha_get_entity_attr_str(invalid_json_obj, "friendly_name");
	munit_assert_null(name);

	name = ha_get_entity_attr_str(test_json_obj, "friendly_name");
	munit_assert_not_null(name);
	munit_assert_string_equal(name, "A sensor always on");

	return MUNIT_OK;
}

MunitResult ha_get_entity_attr_int_test(const MunitParameter params[], void* user_data_or_fixture)
{
	int value;

	value = ha_get_entity_attr_int(NULL, NULL);
	munit_assert_int(value, ==, 0);

	value = ha_get_entity_attr_int(invalid_json_obj, NULL);
	munit_assert_int(value, ==, 0);

	value = ha_get_entity_attr_int(invalid_json_obj, "min_temp");
	munit_assert_int(value, ==, 0);

	value = ha_get_entity_attr_int(climate_json_obj, "min_temp");
	munit_assert_int(value, ==, 7);

	return MUNIT_OK;
}

MunitResult ha_get_entity_attr_array_test(const MunitParameter params[], void* user_data_or_fixture)
{
	char **values;
	int count;

	values = ha_get_entity_attr_array(NULL, NULL, NULL);
	munit_assert_null(values);

	values = ha_get_entity_attr_array(invalid_json_obj, NULL, &count);
	munit_assert_null(values);


	values = ha_get_entity_attr_array(invalid_json_obj, "hvac_modes", &count);
	munit_assert_null(values);

	values = ha_get_entity_attr_array(climate_json_obj, "hvac_modes", NULL);
	munit_assert_null(values);

	values = ha_get_entity_attr_array(climate_json_obj, "hvac_modes", &count);
	munit_assert_not_null(values);
	munit_assert_int(count, ==, 2);
	munit_assert_string_equal(values[0], "heat");
	munit_assert_string_equal(values[1], "off");

	return MUNIT_OK;
}

static int test_register_entity(struct homeassistant *ha,
                           struct json_object *jobj)
{
        return 0;
}

static int test_update_entity(struct homeassistant *ha,
                         struct json_object *jobj)
{
        return 0;
}

static struct ha_callbacks ha_test_callbacks = {
        .register_entity = test_register_entity,
        .update_entity = test_update_entity,
};

MunitResult ha_loop_test(const MunitParameter params[], void* user_data_or_fixture)
{

	ha_test_timer(g_ha);
	ha_loop(g_ha, 100, &ha_test_callbacks);

	return MUNIT_OK;
}

MunitResult ha_exit_test(const MunitParameter params[], void* user_data_or_fixture)
{
	struct homeassistant *ha;
	struct ha_config config;
	int ret;

	ret = load_config(&config);
	munit_assert_int(ret, ==, 0);

	ha_exit(NULL);

	ha = ha_init("https://www.google.com", 443, config.token);
	munit_assert_null(ha);

	ha_exit(ha);

	return MUNIT_OK;
}

MunitTest tests[] = {
	{
		".ha_init", /* name */
		ha_init_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_state", /* name */
		my_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_states", /* name */
		ha_get_states_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_light_turn_on_test", /* name */
		ha_light_turn_on_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_light_turn_off_test", /* name */
		ha_light_turn_off_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_switch_turn_on_test", /* name */
		ha_switch_turn_on_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_switch_turn_off_test", /* name */
		ha_switch_turn_off_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_climate_turn_on_test", /* name */
		ha_climate_turn_on_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_climate_turn_off_test", /* name */
		ha_climate_turn_off_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_entity_id_to_domain_test", /* name */
		ha_entity_id_to_domain_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_friendly_name_test", /* name */
		ha_get_friendly_name_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_entity_id_test", /* name */
		ha_get_entity_id_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_entity_state_test", /* name */
		ha_get_entity_state_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_entity_attr_str_test", /* name */
		ha_get_entity_attr_str_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_entity_attr_int_test", /* name */
		ha_get_entity_attr_int_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_get_entity_attr_array_test", /* name */
		ha_get_entity_attr_array_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_loop_test", /* name */
		ha_loop_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{
		".ha_exit", /* name */
		ha_exit_test, /* test */
		NULL, /* setup */
		NULL, /* tear_down */
		MUNIT_TEST_OPTION_NONE, /* options */
		NULL /* parameters */
	},
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite suite = {
  "tests", /* name */
  tests, /* tests */
  NULL, /* suites */
  1, /* iterations */
  MUNIT_SUITE_OPTION_NONE /* options */
};

int main(int argc, char *argv[])
{
	struct ha_config config;
	int ret;
	int i;

	ret = load_config(&config);
	if (ret)
		return ret;

	test_json_obj = json_tokener_parse(test_json);
	if (!test_json_obj) {
		release_config(&config);
		return -1;
	}

	invalid_json_obj = json_tokener_parse(invalid_json);
	if (!invalid_json_obj) {
		json_object_put(test_json_obj);
		release_config(&config);
		return -1;
	}

	climate_json_obj = json_tokener_parse(climate_json);
	if (!climate_json_obj) {
		json_object_put(test_json_obj);
		json_object_put(invalid_json_obj);
		release_config(&config);
		return -1;
	}

	g_ha = ha_init(config.url, config.port, config.token);
	if (!g_ha) {
		json_object_put(test_json_obj);
		json_object_put(invalid_json_obj);
		json_object_put(climate_json_obj);
		release_config(&config);
		return -1;
	}

	release_config(&config);

	signal(SIGINT, abortHandler);

	ret = munit_suite_main(&suite, NULL, argc, argv);

	json_object_put(test_json_obj);
	json_object_put(invalid_json_obj);
	json_object_put(climate_json_obj);
	ha_exit(g_ha);

	return ret;
}
