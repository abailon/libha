/*
 * Copyright (C) 2022 Alexandre Bailon <abailon@baylibre.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; version 2.1.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef __HA_H__
#define __HA_H__

struct homeassistant;
struct json_object;

typedef int (*register_entity_cb)(struct homeassistant *ha,
				  struct json_object *jobj);
typedef int (*update_entity_cb)(struct homeassistant *ha,
				struct json_object *jobj);

struct ha_callbacks {
	register_entity_cb register_entity;
	update_entity_cb update_entity;
};

/**
 * \brief Allocate and initialized homeassistant instence
 * \arg url the url of homeassistant server to connect to
 * \arg port the to use
 * \arg token the authentication token (please refer to homeassistant documentation for more details)
 * \arg cb TBD
 * \return a pointer to homeassistant or NULL in case of error
 */
struct homeassistant *ha_init(const char *url, int port, const char *token);

/**
 * \brief Release homeassistant resources
 * \arg ha pointer to homeassistant
 */
void ha_exit(struct homeassistant *ha);

/**
 * \brief Check if we are connected and can use home assistant API.
 * \arg ha pointer to homeassistant
 * \return 0 or a negative value in case of error
 */
int ha_check(struct homeassistant *ha);

/**
 * \brief Get state of all entities
 * This get state of all entities and return return json text to be parsed.
 * \arg ha pointer to homeassistant
 * \return entities state in json format or NULL in case of error
 */
char *ha_get_states(struct homeassistant *ha);
/**
 * \brief Get of a entity
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return the entity state or NULL in case of error
 */
const char *ha_get_state(struct homeassistant *ha, const char *entity_id);
int ha_set_state(struct homeassistant *ha, const char *entity_id,
		 const char *state);
/**
 * \brief Run a home assistant service
 * \arg ha pointer to homeassistant
 * \arg domain the name of service domain (please refer to homeassistant documentation for more details)
 * \arg service the name of service to run (please refer to homeassistant documentation for more details)
 * \arg entity_id name of entity to control using the service
 * \arg nargs number of additional arguments
 * \arg ... additional strings. A pair of string is expecting (the key and the value).
 *          The strings are used complete the json service.
 *          See ha_climate_set_fan_mode() to have an example.
 * \return 0 or a negative value in case of error
 */
int ha_service(struct homeassistant *ha, const char *domain,
	       const char *service, const char *entity_id, int nargs, ...);
/**
 * \brief Call light.turn_on service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return 0 or a negative value in case of error
 */
int ha_light_turn_on(struct homeassistant *ha, const char *entity_id);
/**
 * \brief Call light.turn_off service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return 0 or a negative value in case of error
 */
int ha_light_turn_off(struct homeassistant *ha, const char *entity_id);
/**
 * \brief Call switch.turn_on service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return 0 or a negative value in case of error
 */
int ha_switch_turn_on(struct homeassistant *ha, const char *entity_id);
/**
 * \brief Call switch.turn_off service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return 0 or a negative value in case of error
 */
int ha_switch_turn_off(struct homeassistant *ha, const char *entity_id);
/**
 * \brief Call climate.turn_on service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return 0 or a negative value in case of error
 */
int ha_climate_turn_on(struct homeassistant *ha, const char *entity_id);
/**
 * \brief Call climate.turn_off service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \return 0 or a negative value in case of error
 */
int ha_climate_turn_off(struct homeassistant *ha, const char *entity_id);
/**
 * \brief Call climate.set_fan_mode service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \arg fan_mode the fan mode to set (please refer to homeassistant documentation for more details)
 * \return 0 or a negative value in case of error
 */
int ha_climate_set_fan_mode(struct homeassistant *ha, const char *entity_id,
			    const char *fan_mode);
/**
 * \brief Call climate.set_temperature service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \arg temp the temperature to set
 * \return 0 or a negative value in case of error
 */
int ha_climate_set_temp(struct homeassistant *ha, const char *entity_id,
			    const char *temp);
/**
 * \brief Call climate.set_hvac_mode service
 * \arg ha pointer to homeassistant
 * \arg entity_id name of entity
 * \arg mode the HAVC mode to set (please refer to homeassistant documentation for more details)
 * \return 0 or a negative value in case of error
 */
int ha_climate_set_hvac_mode(struct homeassistant *ha, const char *entity_id,
			     const char *mode);

/**
 * \brief Poll the state of all the entities and invoke callback 
 * \arg ha pointer to homeassistant
 * \arg delay delay to before to update state
 * \arg cb pointer to ha_callbacks
 */

void ha_loop(struct homeassistant *ha, long delay, struct ha_callbacks *cb);
/**
 * \brief Stop the ha_loop
 * \arg ha pointer to homeassistant
 */
void ha_stop(struct homeassistant *ha);

/**
 * \brief get the id of the entity
 * \arg jobj the entity object
 * \return the entity id or NULL in case of error
 **/
const char *ha_get_entity_id(struct json_object *jobj);

/**
 * \brief get the state of the entity
 * \arg jobj the entity object
 * \return the entity state or NULL in case of error
 **/
const char *ha_get_entity_state(struct json_object *jobj);

/**
 * \brief get the friendly name of the entity
 * \arg jobj the entity object
 * \return the entity friendly name or NULL in case of error
 **/
const char *ha_get_friendly_name(struct json_object *jobj);

/**
 * \brief get the domain of the entity
 * \arg jobj the entity object
 * \return the entity domain or NULL in case of error
 **/
const char *ha_entity_id_to_domain(const char *entity_id);

/**
 * \brief get the value of an attribute of the entity
 * \arg jobj the entity object
 * \arg attr the name of the attribute to read
 * \ret the attribute value or NULL in case of error
 */
const char *ha_get_entity_attr_str(struct json_object *jobj, const char *attr);

/**
 * \brief get the integer value of an attribute of the entity
 * \arg jobj the entity object
 * \arg attr the name of the attribute to read
 * \ret the integer value read from attribute
 */
int ha_get_entity_attr_int(struct json_object *jobj, const char *attr);

/**
 * \brief get an array of values from an entity's attribute
 * \arg jobj the entity object
 * \arg attr the name of the attribute to read
 * \arg count the number of attribute found in the entity object
 * \ret an array of values or NULL in case of error
 */
char **ha_get_entity_attr_array(struct json_object *jobj, const char *attr,
                                int *count);

#endif /* __HA_H__ */
