/*
 * Copyright (C) 2022 Alexandre Bailon <abailon@baylibre.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <confuse.h>
#include <json-c/json.h>

#include <cmdline.h>
#include <ha.h>

#define FNAME_MAX_SIZE  (256)

struct ha_config {
        char *url;
        int port;
        char *token;
};

static struct homeassistant *g_ha;

static int register_entity(struct homeassistant *ha,
                           struct json_object *jobj)
{
        return 0;
}

static int update_entity(struct homeassistant *ha,
                         struct json_object *jobj)
{
        printf("%s: %s\n", ha_get_entity_id(jobj), ha_get_entity_state(jobj));

        return 0;
}

static struct ha_callbacks ha_callbacks = {
        .register_entity = register_entity,
        .update_entity = update_entity,
};

static void intHandler(int signal) {
    ha_stop(g_ha);
}

char *get_config_file(void)
{
        struct stat st;
        char fname[FNAME_MAX_SIZE];

        sprintf(fname, "%s/.config/ha_client.conf", getenv("HOME"));
        if (stat(fname ,&st) == 0) {
                return strdup(fname);
        }

        if (stat("/etc/ha_client.conf" ,&st) == 0) {
                return strdup("/etc/ha_client.conf");
        }

        return NULL;
}

static int load_config(struct ha_config *config)
{
        cfg_t *cfg;
        char *name = get_config_file();
        cfg_opt_t opts[] = {
                CFG_STR("url", "", CFGF_NONE),
                CFG_INT("port", 443, CFGF_NONE),
                CFG_STR("token", "", CFGF_NONE),
                CFG_END()
        };

        cfg = cfg_init(opts, CFGF_NONE);
        if (cfg_parse(cfg, name) == CFG_PARSE_ERROR)
                return 1;

        config->url = strdup(cfg_getstr(cfg, "url"));
        config->port = cfg_getint(cfg, "port");
        config->token = strdup(cfg_getstr(cfg, "token"));

        free(name);
        cfg_free(cfg);

        return 0;
}

static void release_config(struct ha_config *config)
{
        free(config->url);
        free(config->token);
}

int main(int argc, char *argv[])
{
        struct gengetopt_args_info main_args_info;
        struct ha_config config;
        int ret;
        int i;

        if (cmdline_parser(argc, argv, &main_args_info) != 0)
                return 1;

        ret = load_config(&config);
        if (ret)
                return ret;

        signal(SIGINT, intHandler);

        g_ha = ha_init(config.url, config.port, config.token);
        if (!g_ha)
                return -1;

        if (main_args_info.list_given) {
                const char *domain = main_args_info.list_arg;
                struct json_object *array;
                int arraylen;
                char *data;

                data = ha_get_states(g_ha);
                if (!data)
                        return -1;

                array = json_tokener_parse(data);
                free(data);

                arraylen = json_object_array_length(array);
                for (i =0; i < arraylen; i++) {
                        struct json_object *jobj;
                        const char *entity_id;

                        jobj = json_object_array_get_idx(array, i);
                        entity_id = ha_get_entity_id(jobj);

                        if (!strcmp("all", domain)) {
                                printf("%s\n", entity_id);
                        } else if (!strncmp(domain, entity_id, strlen(domain))) {
                                printf("%s\n", entity_id);
                        }
                }
        }

        if (main_args_info.turn_on_given) {
                for (i = 0; i < main_args_info.turn_on_given; i++) {
                        const char *entity_id = main_args_info.turn_on_arg[i];
                        const char *domain = ha_entity_id_to_domain(entity_id);

                        if (!domain) {
                                printf("Failed to found a domain for %s\n",
                                        entity_id);
                                continue;
                        }

                        ha_service(g_ha, domain, "turn_on", entity_id, 0);
                }
        }

        if (main_args_info.turn_off_given) {
                for (i = 0; i < main_args_info.turn_off_given; i++) {
                        const char *entity_id = main_args_info.turn_off_arg[i];
                        const char *domain = ha_entity_id_to_domain(entity_id);

                        if (!domain) {
                                printf("Failed to found a domain for %s\n",
                                        entity_id);
                                continue;
                        }

                        ha_service(g_ha, domain, "turn_off", entity_id, 0);
                }
        }

        if (main_args_info.state_given) {
                for (i = 0; i < main_args_info.state_given; i++) {
                        const char *entity_id = main_args_info.state_arg[i];

                        printf ("%s: %s\n", entity_id,
                                ha_get_state(g_ha, entity_id));
                }
        }

//        ha_loop(g_ha, 1000000);
        ha_exit(g_ha);

        return 0;
}
