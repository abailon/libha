/*
 * Copyright (C) 2022 Alexandre Bailon <abailon@baylibre.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; version 2.1.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <errno.h>
#include <stdarg.h>
#include <string.h>

#include <unistd.h>

#include <rest_client.h>
#include <json-c/json.h>

#include <ha.h>

struct homeassistant {
	RestClient c;
	char *token;
	int loop;
};

static const char *domains[] = {
	"climate",
	"switch",
	"light",
	"sensor",
	"media_player",
	"binary_sensor",
	"sun",
	"zone",
	"scene",
	"update",
	"person",
	NULL,
};

int ha_check(struct homeassistant *ha)
{
	int ret = 0;
	RestRequest req;
	RestResponse res;
	RestFilter* chain = NULL;

	struct json_object *parsed_json;
	struct json_object *message;
	const char *expected_message = "API running.";

	if (!ha)
		return -EINVAL;

	RestRequest_init(&req, "/", HTTP_GET);
	RestRequest_add_header(&req, "Content-Type: application/json");
	RestRequest_add_header(&req, ha->token);

	RestResponse_init(&res);

	chain = RestFilter_add(chain, &RestFilter_execute_curl_request);
	RestClient_execute_request(&ha->c, chain, &req, &res);

	if (!res.body) {
		ret = ENOTCONN;
		goto err_network;
	}

	parsed_json = json_tokener_parse(res.body);
	json_object_object_get_ex(parsed_json, "message", &message);
	if (!message || strcmp(json_object_get_string(message), expected_message)) {
		printf ("%s\n", res.body);
		ret = -EINVAL;
	}

	json_object_put(parsed_json);

err_network:
	RestFilter_free(chain);
	RestResponse_destroy(&res);
	RestRequest_destroy(&req);

	return ret;
}

char *ha_get_states(struct homeassistant *ha)
{
	RestRequest req;
	RestResponse res;
	RestFilter* chain = NULL;
	char *body = NULL;

	if (!ha)
		return NULL;

	RestRequest_init(&req, "/states", HTTP_GET);
	RestRequest_add_header(&req, "Content-Type: application/json");
	RestRequest_add_header(&req, ha->token);

	RestResponse_init(&res);

	chain = RestFilter_add(chain, &RestFilter_execute_curl_request);
	RestClient_execute_request(&ha->c, chain, &req, &res);

	if (!res.body)
		goto err_network;

	body = malloc(strlen(res.body) + 1);
	strcpy(body, res.body);

err_network:
	RestFilter_free(chain);
	RestResponse_destroy(&res);
	RestRequest_destroy(&req);

	return body;
}

const char *ha_get_entity_id(struct json_object *jobj)
{
	struct json_object *entity_id_obj;
	json_object_object_get_ex(jobj, "entity_id", &entity_id_obj);

	return json_object_get_string(entity_id_obj);
}

const char *ha_get_entity_state(struct json_object *jobj)
{
	struct json_object *state_obj;
	json_object_object_get_ex(jobj, "state", &state_obj);

	return json_object_get_string(state_obj);
}

const char *ha_get_friendly_name(struct json_object *jobj)
{
	struct json_object *attributes_obj;
	struct json_object *friendly_name_obj;

	json_object_object_get_ex(jobj, "attributes", &attributes_obj);
	json_object_object_get_ex(attributes_obj, "friendly_name",
				  &friendly_name_obj);

	return json_object_get_string(friendly_name_obj);
}

const char *ha_entity_id_to_domain(const char *entity_id)
{
	int i = 0;

	if (!entity_id)
		return NULL;

	while (domains[i]) {
		if (!strncmp(domains[i], entity_id, strlen(domains[i])))
			return domains[i];
		i++;
	}

	return NULL;
}

const char *ha_get_entity_attr_str(struct json_object *jobj, const char *attr)
{
	struct json_object *attributes_obj;
	struct json_object *name_obj;

	json_object_object_get_ex(jobj, "attributes", &attributes_obj);
	json_object_object_get_ex(attributes_obj, attr, &name_obj);

	return json_object_get_string(name_obj);
}

int ha_get_entity_attr_int(struct json_object *jobj, const char *attr)
{
	struct json_object *attributes_obj;
	struct json_object *name_obj;

	json_object_object_get_ex(jobj, "attributes", &attributes_obj);
	json_object_object_get_ex(attributes_obj, attr, &name_obj);

	return json_object_get_int(name_obj);
}

char **ha_get_entity_attr_array(struct json_object *jobj, const char *attr,
				int *count)
{
	struct json_object *attributes_obj;
	struct json_object *array_id_obj;
	char **array;
	int arraylen;
	int ret;
	int i;

	if (!count)
		return NULL;

	json_object_object_get_ex(jobj, "attributes", &attributes_obj);
	ret = json_object_object_get_ex(attributes_obj, attr, &array_id_obj);
	if (!ret)
		return NULL;

	arraylen = json_object_array_length(array_id_obj);
	array = malloc(sizeof(char *) * arraylen);
	for (i = 0; i < arraylen; i++) {
		const char *tmp_char;
		struct json_object *tmp_obj = json_object_array_get_idx(array_id_obj, i);
		tmp_char = json_object_get_string(tmp_obj);
		array[i] = malloc(strlen(tmp_char) + 1);
		strcpy(array[i], tmp_char);
	}

	*count = arraylen;
	return array;
}

static struct json_object *find_entity(struct json_object *array,
				const char *entity_id)
{
	int i;
	int arraylen;

	if (!array)
		return NULL;

	arraylen = json_object_array_length(array);
	for (i = 0; i < arraylen; i++) {
		struct json_object *jobj = json_object_array_get_idx(array, i);
		const char *_entity_id = ha_get_entity_id(jobj);

		if (!strcmp(_entity_id, entity_id))
			return jobj;
	}

	return NULL;
}

static const char *get_new_entity_state(struct json_object *prev_jobj,
				 struct json_object *jobj)
{
	const char *prev_state = ha_get_entity_state(prev_jobj);
	const char *state = ha_get_entity_state(jobj);

	if (strcmp(prev_state, state))
		return state;

	return NULL;
}

static int ha_register_entity(struct homeassistant *ha, struct ha_callbacks *cb,
			      struct json_object *jobj)
{
	if (!cb || !cb->register_entity)
		return 0;

	if (!ha)
		return -EINVAL;

	return cb->register_entity(ha, jobj);
}

static int ha_update_entity(struct homeassistant *ha, struct ha_callbacks *cb,
			    struct json_object *jobj)
{
	if (!cb || !cb->register_entity)
		return 0;

	if (!ha)
		return -EINVAL;

	return cb->update_entity(ha, jobj);
}

static struct json_object *ha_get_states_update(struct homeassistant *ha, 
						struct json_object *prev_array,
						struct ha_callbacks *cb)
{
	int i;
	char *data;
	struct json_object *array;
	int arraylen;

	data = ha_get_states(ha);
	if (!data)
		return NULL;

	array = json_tokener_parse(data);
	free(data);

	arraylen = json_object_array_length(array);
	for (i =0; i < arraylen; i++) {
		const char *state;
		struct json_object *jobj = json_object_array_get_idx(array, i);
		const char *entity_id = ha_get_entity_id(jobj);
		struct json_object *prev_jobj = find_entity(prev_array,
							    entity_id);

		if (!prev_jobj) {
			printf ("Adding a new entity: %s\n", entity_id);
			ha_register_entity(ha, cb, jobj);
			continue;
		}

		state = get_new_entity_state(prev_jobj, jobj);
		if (!state)
			continue;

		printf("Updating state of %s\n", entity_id);
		ha_update_entity(ha, cb, jobj);
	}

	if (prev_array)
		json_object_put(prev_array);

	return array;
}

int ha_service(struct homeassistant *ha, const char *domain,
	       const char *service, const char *entity_id, int nargs, ...)
{
	RestRequest req;
	RestResponse res;
	RestFilter* chain = NULL;
	char *uri = malloc(128);
	char *body = malloc(128);
	va_list argp;

	if (!ha || !entity_id)
		return -EINVAL;

	sprintf(uri, "/services/%s/%s", domain, service);
	if (!nargs)
		sprintf(body, "{\"entity_id\" : \"%s\"}", entity_id);
	else {
		sprintf(body, "{\"entity_id\" : \"%s\"", entity_id);
		va_start(argp, nargs);
		for (int i = 0; i < nargs / 2; i++) {
			char buffer[32];
			char *name, *value;

			name = va_arg(argp, char *);
			value = va_arg(argp, char *);
			sprintf(buffer, ", \"%s\" : \"%s\"", name, value);
			strcat(body, buffer);
		}
		strcat(body, "}");
	}

	RestRequest_init(&req, uri, HTTP_POST);
	RestRequest_add_header(&req, ha->token);\
	RestRequest_set_array_body(&req, body, strlen(body),
				   "application/json");

	RestResponse_init(&res);

	chain = RestFilter_add(chain, &RestFilter_execute_curl_request);
	RestClient_execute_request(&ha->c, chain, &req, &res);

	//TODO handle the response

	RestFilter_free(chain);
	RestResponse_destroy(&res);
	RestRequest_destroy(&req);

	free(body);
	free(uri);

	return 0;
}

int ha_switch_turn_on(struct homeassistant *ha, const char *entity_id)
{
	return ha_service(ha, "switch", "turn_on", entity_id, 0);
}

int ha_switch_turn_off(struct homeassistant *ha, const char *entity_id)
{
	return ha_service(ha, "switch", "turn_off", entity_id, 0);
}

int ha_light_turn_on(struct homeassistant *ha, const char *entity_id)
{
	return ha_service(ha, "light", "turn_on", entity_id, 0);
}

int ha_light_turn_off(struct homeassistant *ha, const char *entity_id)
{
	return ha_service(ha, "light", "turn_off", entity_id, 0);
}

int ha_climate_turn_on(struct homeassistant *ha, const char *entity_id)
{
	return ha_service(ha, "climate", "turn_on", entity_id, 0);
}

int ha_climate_turn_off(struct homeassistant *ha, const char *entity_id)
{
	return ha_service(ha, "climate", "turn_off", entity_id, 0);
}

int ha_climate_set_fan_mode(struct homeassistant *ha, const char *entity_id,
			    const char *fan_mode)
{
	return ha_service(ha, "climate", "set_fan_mode", entity_id, 2,
			  "fan_mode", fan_mode);
}

int ha_climate_set_temp(struct homeassistant *ha, const char *entity_id,
			const char *temp)
{
	return ha_service(ha, "climate", "set_temperature", entity_id, 2,
			  "temperature", temp);
}

int ha_climate_set_hvac_mode(struct homeassistant *ha, const char *entity_id,
			     const char *mode)
{
	return ha_service(ha, "climate", "set_hvac_mode", entity_id, 2,
			  "hvac_mode", mode);
}

int ha_set_state(struct homeassistant *ha, const char *entity_id,
		 const char *state)
{
	RestRequest req;
	RestResponse res;
	RestFilter* chain = NULL;
	char uri[128];
	char body[128];

	if (!ha)
		return -EINVAL;

	sprintf(uri, "/states/%s", entity_id);
	sprintf(body, "{\"state\" : \"%s\"}", state);
	RestRequest_init(&req, uri, HTTP_POST);
	RestRequest_add_header(&req, ha->token);\
	RestRequest_set_array_body(&req, body, strlen(body),
				   "application/json");

	RestResponse_init(&res);

	chain = RestFilter_add(chain, &RestFilter_execute_curl_request);
	RestClient_execute_request(&ha->c, chain, &req, &res);

	//TODO handle the response

	RestFilter_free(chain);
	RestResponse_destroy(&res);
	RestRequest_destroy(&req);

	return 0;
}

const char *ha_get_state(struct homeassistant *ha, const char *entity_id)
{
	RestRequest req;
	RestResponse res;
	RestFilter* chain = NULL;
	struct json_object *jobj;
	char *body = NULL;
	char uri[128];

	if (!ha)
		return NULL;

	sprintf(uri, "/states/%s", entity_id);
	RestRequest_init(&req, uri, HTTP_GET);
	RestRequest_add_header(&req, "Content-Type: application/json");
	RestRequest_add_header(&req, ha->token);

	RestResponse_init(&res);

	chain = RestFilter_add(chain, &RestFilter_execute_curl_request);
	RestClient_execute_request(&ha->c, chain, &req, &res);

	if (!res.body)
		goto err_network;

	body = malloc(strlen(res.body) + 1);
	strcpy(body, res.body);

err_network:
	RestFilter_free(chain);
	RestResponse_destroy(&res);
	RestRequest_destroy(&req);

	if (!body)
		return NULL;

	jobj = json_tokener_parse(body);
	return ha_get_entity_state(jobj);
}

struct homeassistant *ha_init(const char *url, int port, const char *token)
{
	struct homeassistant *ha;
	const char *auth_base = "Authorization: Bearer ";

	if (!url || !port || !token)
		return NULL;

	ha = malloc(sizeof(*ha));
	if (!ha)
		return NULL;

	ha->loop = 1;
	ha->token = malloc(strlen(token) + strlen(auth_base) + 1);
	if (!ha->token) {
		free(ha);
		return NULL;
	}
	sprintf(ha->token, "%s%s", auth_base, token);

	curl_global_init(CURL_GLOBAL_DEFAULT);

	RestClient_init(&ha->c, url, port);

	if (ha_check(ha)) {
		printf("Failed to connect to homeassistant\n");
		ha_exit(ha);
		return NULL;
	}

	return ha;
}

void ha_loop(struct homeassistant *ha, long delay, struct ha_callbacks *cb)
{
        struct json_object *array = NULL;
        struct json_object *old_array = NULL;

        if (!ha)
		return;

        while (ha->loop) {
                usleep(delay);

                array = ha_get_states_update(ha, old_array, cb);
                if (array == NULL) {
                        printf("connection lost!\n");
                } else {
                        old_array = array;
                }
        }
}

void ha_stop(struct homeassistant *ha)
{
	if (!ha)
		return;

	ha->loop = 0;
}

void ha_exit(struct homeassistant *ha)
{
	if (!ha)
		return;

	RestClient_destroy(&ha->c);

	free(ha->token);
	free(ha);

	curl_global_cleanup();
}
